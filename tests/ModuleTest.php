<?php

namespace ServiceCore\Log\Test;

use Laminas\EventManager\EventManager;
use Laminas\EventManager\SharedEventManager;
use Laminas\Mvc\Application;
use Laminas\Mvc\MvcEvent;
use Laminas\ServiceManager\ServiceManager;
use PHPUnit\Framework\TestCase;
use ServiceCore\Log\Listener\Log as LogListener;
use ServiceCore\Log\Module;

class ModuleTest extends TestCase
{
    public function testGetConfig(): void
    {
        $module = new Module();
        $config = $module->getConfig();

        $this->assertArrayHasKey('service_manager', $config);
        $this->assertArrayHasKey('factories', $config['service_manager']);
    }

    public function testOnBootstrapThrowsExceptionIfSharedManagerNotSet(): void
    {
        $module  = new Module();
        $builder = $this->getMockBuilder(MvcEvent::class);

        $builder->disableOriginalConstructor();

        $builder->setMethods(
            [
                'getApplication'
            ]
        );

        $event   = $builder->getMock();
        $builder = $this->getMockBuilder(Application::class);

        $builder->disableOriginalConstructor();

        $builder->setMethods(
            [
                'getServiceManager',
                'getEventManager'
            ]
        );

        $application    = $builder->getMock();
        $serviceManager = new ServiceManager();
        $builder        = $this->getMockBuilder(LogListener::class);

        $builder->disableOriginalConstructor();

        $builder->setMethods(
            [
                'attachShared'
            ]
        );

        $logListener = $builder->getMock();

        $logListener->expects($this->never())
                    ->method('attachShared')
                    ->willReturn(null);

        $serviceManager->setService(LogListener::class, $logListener);

        $application->expects($this->once())
                    ->method('getServiceManager')
                    ->willReturn($serviceManager);

        $application->expects($this->once())
                    ->method('getEventManager')
                    ->willReturn(new EventManager());

        $event->expects($this->once())
              ->method('getApplication')
              ->willReturn($application);

        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('SharedManager not set on Application EventManager');

        $module->onBootstrap($event);
    }

    public function testOnBootstrap(): void
    {
        $module  = new Module();
        $builder = $this->getMockBuilder(MvcEvent::class);

        $builder->disableOriginalConstructor();

        $builder->setMethods(
            [
                'getApplication'
            ]
        );

        $event   = $builder->getMock();
        $builder = $this->getMockBuilder(Application::class);

        $builder->disableOriginalConstructor();

        $builder->setMethods(
            [
                'getServiceManager',
                'getEventManager'
            ]
        );

        $application    = $builder->getMock();
        $serviceManager = new ServiceManager();
        $builder        = $this->getMockBuilder(LogListener::class);

        $builder->disableOriginalConstructor();

        $builder->setMethods(
            [
                'attachShared'
            ]
        );

        $logListener = $builder->getMock();

        $logListener->expects($this->once())
                    ->method('attachShared')
                    ->willReturn(null);

        $serviceManager->setService(LogListener::class, $logListener);

        $application->expects($this->once())
                    ->method('getServiceManager')
                    ->willReturn($serviceManager);

        $shared       = new SharedEventManager();
        $eventManager = new EventManager($shared);

        $application->expects($this->once())
                    ->method('getEventManager')
                    ->willReturn($eventManager);

        $event->expects($this->once())
              ->method('getApplication')
              ->willReturn($application);

        $module->onBootstrap($event);
    }
}
