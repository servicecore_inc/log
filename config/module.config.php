<?php

namespace ServiceCore\Log;

use Laminas\Log\Writer\Stream;
use ServiceCore\Log\Adapter\Factory\Log as LogAdapterFactory;
use ServiceCore\Log\Adapter\Log as LogAdapter;
use ServiceCore\Log\Factory\Stream as StreamFactory;
use ServiceCore\Log\Listener\Factory\Log as LogListenerFactory;
use ServiceCore\Log\Listener\Log as LogListener;

return [
    'service_manager' => [
        'factories' => [
            Stream::class      => StreamFactory::class,
            LogAdapter::class  => LogAdapterFactory::class,
            LogListener::class => LogListenerFactory::class
        ]
    ]
];
