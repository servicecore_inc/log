<?php

namespace ServiceCore\Log\Helper;

use ServiceCore\Log\Adapter\Log;

trait LoggerAwareTrait
{
    /** @var Log|null */
    private $logger;

    public function getLogger(): ?Log
    {
        return $this->logger;
    }

    public function setLogger(Log $logger): self
    {
        $this->logger = $logger;

        return $this;
    }
}
