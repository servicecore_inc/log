<?php

namespace ServiceCore\Log\Factory;

use Interop\Container\ContainerInterface;
use InvalidArgumentException;
use Laminas\Log\Writer\Stream as StreamWriter;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\Log\Exception\InvalidConfigurationException;

class Stream implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): StreamWriter
    {
        $config = $container->get('Config')['log']['writers']['stream'];

        if (!\array_key_exists(StreamWriter::class, $config)) {
            throw new InvalidConfigurationException(
                'Missing Stream config from log key. Expected $config[\'log\'][\Laminas\Log\Writer\Stream::class]'
            );
        }

        $config = $config[StreamWriter::class];

        return new StreamWriter($this->getFile($config['options']['path']));
    }

    private function getFile($path): string
    {
        if (!\is_dir($path)) {
            throw new InvalidArgumentException(\sprintf('%s is not a valid log path', $path));
        }

        $file = \date('Ymd') . '.txt';

        $filePath = $path . DIRECTORY_SEPARATOR . $file;

        if (!\file_exists($filePath)) {
            $fh = \fopen($filePath, 'wb+');
            \fclose($fh);
            \chmod($filePath, 0777);
        }

        return $filePath;
    }
}
