<?php

namespace ServiceCore\Log\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use RuntimeException;
use ServiceCore\Log\Adapter\Log as LogAdapter;
use ServiceCore\Log\RoleData\LoggerAwareInterface;

class LoggerDelegator implements DelegatorFactoryInterface
{
    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null): object
    {
        $class = $callback();

        if (!$class instanceof LoggerAwareInterface) {
            throw new RuntimeException(
                \sprintf(
                    'Class %s is not an instance of LoggerAwareInterface',
                    \get_class($class)
                )
            );
        }

        /** @var LogAdapter $logger */
        $logger = $container->get(LogAdapter::class);

        $class->setLogger($logger);

        return $class;
    }
}
