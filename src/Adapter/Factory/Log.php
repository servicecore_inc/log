<?php

namespace ServiceCore\Log\Adapter\Factory;

use Interop\Container\ContainerInterface;
use Laminas\Log\Logger;
use Laminas\Log\Writer\WriterInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use RuntimeException;
use ServiceCore\Log\Adapter\Log as LogAdapter;
use ServiceCore\Log\Exception\MissingWriterException;
use ServiceCore\Log\Exception\WriterNotFoundException;

class Log implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): LogAdapter
    {
        /** @var array $config */
        $config = $container->get('Config')['log'] ?? [];
        $logger = new Logger();

        $this->validateConfig($config);
        $this->addWriters($container, $logger, $config);

        return new LogAdapter($logger);
    }

    private function validateConfig(array $config): void
    {
        if (!\array_key_exists('writers', $config)) {
            throw new RuntimeException('Key `writers` not found in config');
        }

        if (!\array_key_exists('stream', $config['writers'])) {
            throw new RuntimeException('Key `stream` not found in $config[\'writers\']');
        }

        if (!\is_array($config['writers']['stream'])) {
            throw new RuntimeException('$config[\'writers\'][\'streams\'] is not an array');
        }
    }

    private function addWriters(ContainerInterface $container, Logger $logger, array $config): void
    {
        foreach ($config['writers']['stream'] as $writerConfig) {
            $logger->addWriter($this->getWriter($container, $writerConfig), $this->getPriority($writerConfig));
        }

        if ($logger->getWriters()->count() < 1) {
            throw new MissingWriterException('Logger must have at least 1 writer configured');
        }
    }

    private function getWriter(ContainerInterface $container, array $config): WriterInterface
    {
        $writerName = $config['name'] ?? null;

        if ($writerName === null || !$container->has($writerName)) {
            throw new WriterNotFoundException('No writers found');
        }

        return $container->get($writerName);
    }

    private function getPriority(array $config): ?int
    {
        if (!\array_key_exists('priority', $config) || !\is_numeric($config['priority'])) {
            return null;
        }

        return (int)$config['priority'];
    }
}
