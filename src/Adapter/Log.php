<?php

namespace ServiceCore\Log\Adapter;

use Laminas\Log\Logger;
use Laminas\Log\Writer\Stream as StreamWriter;
use Laminas\Log\Writer\WriterInterface;
use RuntimeException;

class Log
{
    // Constants are in here just so we don't have to import the Zend logger wherever the adapter is used
    public const EMERG  = Logger::EMERG;
    public const ALERT  = Logger::ALERT;
    public const CRIT   = Logger::CRIT;
    public const ERR    = Logger::ERR;
    public const WARN   = Logger::WARN;
    public const NOTICE = Logger::NOTICE;
    public const INFO   = Logger::INFO;
    public const DEBUG  = Logger::DEBUG;

    /** @var Logger */
    protected $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function log(int $priority, $message): void
    {
        $this->logger->log($priority, $message);
    }

    public function addStreamWriter(string $filePath, int $priority = 1): void
    {
        if (!\file_exists($filePath)) {
            if (!\touch($filePath)) {
                throw new RuntimeException(\sprintf('Could not create file %s', $filePath));
            }

            if (!\chmod($filePath, 0777)) {
                throw new RuntimeException(\sprintf('Could not chmod file %s to 0777', $filePath));
            }
        }

        $this->logger->addWriter(new StreamWriter($filePath), $priority);
    }

    public function shutdownStreamWriters(): void
    {
        /** @var WriterInterface $writer */
        foreach ($this->logger->getWriters() as $writer) {
            if ($writer instanceof StreamWriter) {
                $writer->shutdown();
            }
        }
    }
}
