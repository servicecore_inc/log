<?php

namespace ServiceCore\Log\RoleData;

use ServiceCore\Log\Adapter\Log;

interface LoggableInterface
{
    public function log(Log $logger);
}
