<?php

namespace ServiceCore\Log\RoleData;

use ServiceCore\Log\Adapter\Log;

interface LoggerAwareInterface
{
    public function getLogger(): ?Log;

    public function setLogger(Log $logger);
}
