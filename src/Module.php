<?php

namespace ServiceCore\Log;

use Laminas\EventManager\SharedEventManager;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Laminas\Mvc\MvcEvent;
use RuntimeException;
use ServiceCore\Log\Listener\Log as LogListener;

class Module implements ConfigProviderInterface
{
    public function getConfig(): array
    {
        return require \dirname(__DIR__) . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $event): void
    {
        $application    = $event->getApplication();
        $serviceManager = $application->getServiceManager();

        /** @var LogListener $logListener */
        $logListener = $serviceManager->get(LogListener::class);

        /** @var SharedEventManager $shared */
        $shared = $application->getEventManager()->getSharedManager();

        if ($shared === null) {
            throw new RuntimeException('SharedManager not set on Application EventManager');
        }

        $logListener->attachShared($shared);
    }
}
