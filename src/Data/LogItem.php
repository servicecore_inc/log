<?php

namespace ServiceCore\Log\Data;

use ServiceCore\Log\Adapter\Log;
use ServiceCore\Log\RoleData\LoggableInterface;

class LogItem implements LoggableInterface
{
    /** @var string */
    private $message;

    /** @var string */
    private $priority;

    public function __construct($message = '', $priority = '')
    {
        $this->message  = $message;
        $this->priority = $priority;
    }

    public function getLogMessage(): string
    {
        return $this->message;
    }

    public function getLogPriority(): string
    {
        return $this->priority;
    }

    public function setLogMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function setLogPriority(string $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function log(Log $logger): void
    {
        $logger->log(
            $this->getLogPriority(),
            $this->getLogMessage()
        );
    }
}
