<?php

namespace ServiceCore\Log\Listener\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\Log\Adapter\Log as LogAdapter;
use ServiceCore\Log\Listener\Log as LogListener;

class Log implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): LogListener
    {
        /** @var LogAdapter $logger */
        $logger = $container->get(LogAdapter::class);

        return new LogListener($logger);
    }
}
