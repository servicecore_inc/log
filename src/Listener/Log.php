<?php

namespace ServiceCore\Log\Listener;

use Laminas\EventManager\Event;
use Laminas\EventManager\SharedEventManagerInterface;
use ServiceCore\Log\Adapter\Log as LogAdapter;
use ServiceCore\Log\RoleData\LoggableInterface;

class Log
{
    /** @var LogAdapter */
    private $logger;

    public function __construct(LogAdapter $logger)
    {
        $this->logger = $logger;
    }

    public function attachShared(SharedEventManagerInterface $events): void
    {
        $events->attach(
            '*',
            '*',
            [$this, 'log'],
            999999
        );
    }

    public function log(Event $event): void
    {
        $param = $event->getParams();
        if ($param instanceof LoggableInterface) {
            $param->log($this->logger);
        }
    }
}
